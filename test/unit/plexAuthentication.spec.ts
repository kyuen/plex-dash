import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import sinon = require("sinon");
import sinonChai = require("sinon-chai");
import cache = require("memory-cache");
import { PlexAuthenticator } from "../../lib/plexAuthentication";
chai.use(chaiAsPromised);
chai.use(sinonChai);
const expect = chai.expect;
let authenticator;
let requestMock;
before(() => {
	requestMock = {
		post: sinon.stub().resolves({})
	};
});

describe("Plex Authentication API", () => {
	describe("when authenticating with Plex", () => {
		beforeEach(() => {
			requestMock.post.reset();
			cache.clear();
			authenticator = new PlexAuthenticator(cache, requestMock);
		});
		it("caches and returns a plex token for the given username and password", () => {
			const expectedToken = "token";
			const expectedPlexResponse = { body: { user: { authToken: expectedToken } } };
			requestMock.post.resolves(expectedPlexResponse);
			return authenticator.authenticate({ username: "test", password: "password" }).then(() => {
				authenticator.authenticate({ username: "test", password: "password" }).then(() => {
					authenticator.authenticate({ username: "test", password: "password" }).then(() => {
						return authenticator.authenticate({ username: "test", password: "password" }).then((token) => {
							expect(token).to.be.a("string");
							expect(token).to.equal(expectedToken);
							expect(requestMock.post).to.have.been.calledOnce;
						});
					});
				});
			});
		});
		it("caches and returns a given plex token", () => {
			const expectedToken = "token";
			authenticator.authenticate({ authToken: expectedToken });
			authenticator.authenticate({ authToken: expectedToken });
			authenticator.authenticate({ authToken: expectedToken });
			return authenticator.authenticate({ authToken: expectedToken }).then((token) => {
				expect(token).to.be.a("string");
				expect(token).to.equal(expectedToken);
				expect(requestMock.post).to.not.have.been.called;
			});
		});
		it("throws an error if plex.tv returns an error", () => {
			const error = new Error("Some error");
			requestMock.post.rejects(error);
			return expect(authenticator.authenticate({ username: "test", password: "password" }))
				.to.be.rejectedWith(error);
		});
	});
});
