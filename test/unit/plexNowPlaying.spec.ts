import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import sinon = require("sinon");
import sinonChai = require("sinon-chai");
import { PlexAuthenticator } from "../../lib/plexAuthentication";
import { NowPlaying } from "../../lib/plexNowPlaying";
import { MediaMetadata, MediaType, MediaUser, SessionItem } from "../../lib/plexTypes";
chai.use(chaiAsPromised);
chai.use(sinonChai);
const expect = chai.expect;
let nowPlayingProxy;
let requestMock;
let authenticatorMock;
let configMock;

describe("Plex Now Playing API", () => {
	beforeEach(() => {
		configMock = {
			get: () => "test-config-value"
		};
		requestMock = {
			get: sinon.stub().resolves({})
		};
		authenticatorMock = new PlexAuthenticator({ get: () => "", put: () => ""}, requestMock);
		authenticatorMock.getToken = sinon.stub().resolves("token");
		nowPlayingProxy = new NowPlaying(authenticatorMock, requestMock, configMock);
	});
	describe("when getting currently playing media", () => {
		it("uses default server 'localhost:32400' if server configuration doesn't exist", () => {
			configMock.get = () => null;
			let requestOptions = { uri: "" };
			requestMock.get.callsFake((options) => {
				requestOptions = options;
				return Promise.resolve({});
			});
			return nowPlayingProxy.get().then(() => {
				expect(requestOptions.uri).to.contain("localhost:32400");
			});
		});
		it("uses authentication proxy to retrieve an authentication token", () => {
			return nowPlayingProxy.get().then(() => {
				expect(authenticatorMock.getToken).to.have.been.calledOnce;
			});
		});
		it("throws an error when the authentication proxy is unable to return an authentication token", () => {
			authenticatorMock.getToken.resolves(null);
			return expect(nowPlayingProxy.get()).to.be.rejected;
		});
		it("returns translated media information if streaming a movie", () => {
			const expectedMovie = getExpectedMovie();
			requestMock.get.resolves({
				body: {
					MediaContainer: {
						Video: [ getMockPlexVideoItem(expectedMovie, "movie") ]
					}
				}
			});
			return nowPlayingProxy.get().then((mediaItems) => {
				expect(mediaItems).to.be.an("Array");
				expect(mediaItems[0]).to.deep.equal(expectedMovie);
			});
		});
		it("returns translated media information if streaming a tv show", () => {
			const expectedTvShow = getExpectedTvShow();
			requestMock.get.resolves({
				body: {
					MediaContainer: {
						Video: [ getMockPlexVideoItem(expectedTvShow, "episode") ]
					}
				}
			});
			return nowPlayingProxy.get().then((mediaItems) => {
				expect(mediaItems).to.be.an("Array");
				expect(mediaItems[0]).to.deep.equal(expectedTvShow);
			});
		});
		it("returns translated media information if streaming a song", () => {
			const expectedSong = getExpectedSong();
			requestMock.get.resolves({
				body: {
					MediaContainer: {
						Video: [ getMockPlexVideoItem(expectedSong, "track") ]
					}
				}
			});
			return nowPlayingProxy.get().then((mediaItems) => {
				expect(mediaItems).to.be.an("Array");
				expect(mediaItems[0]).to.deep.equal(expectedSong);
			});
		});
		it("returns translated media information if multiple items are being streamed", () => {
			const expectedTvShow = getExpectedTvShow();
			const expectedMovie = getExpectedMovie();
			const expectedSong = getExpectedSong();
			requestMock.get.resolves({
				body: {
					MediaContainer: {
						Video: [
							getMockPlexVideoItem(expectedMovie, "movie"),
							getMockPlexVideoItem(expectedTvShow, "episode"),
							getMockPlexVideoItem(expectedSong, "track")
						]
					}
				}
			});
			return nowPlayingProxy.get().then((mediaItems) => {
				expect(mediaItems).to.be.an("Array");
				expect(mediaItems.length).to.be.equal(3);
				expect(mediaItems).to.include(expectedTvShow);
				expect(mediaItems).to.include(expectedMovie);
				expect(mediaItems).to.include(expectedSong);
			});
		});
		it("returns an empty array if no media is streaming", () => {
			requestMock.get.resolves({
				body: { MediaContainer: { size: 0 } }
			});
			return nowPlayingProxy.get().then((mediaItems) => {
				expect(mediaItems).to.be.an("Array");
				expect(mediaItems).to.be.empty;
			});
		});
		it("returns an empty array if there is an issue calling plex", () => {
			const error = new Error("Issue calling Plex");
			requestMock.get.rejects(error);
			return nowPlayingProxy.get().then((mediaItems) => {
				expect(mediaItems).to.be.an("Array");
				expect(mediaItems).to.be.empty;
			});
		});
	});
});

function getMockPlexVideoItem(sessionItem: SessionItem, mediaType: string) {
	const plexVideoItem = {
		User: {
			id: sessionItem.user.id,
			thumb: sessionItem.user.thumb,
			title: sessionItem.user.name
		},
		duration: sessionItem.mediaInfo.duration,
		grandparentTitle: sessionItem.mediaInfo.grandparentTitle,
		key: sessionItem.mediaInfo.key,
		librarySectionId: sessionItem.mediaInfo.librarySectionId,
		librarySectionKey: sessionItem.mediaInfo.librarySectionKey,
		parentTitle: sessionItem.mediaInfo.parentTitle,
		title: sessionItem.mediaInfo.title,
		type: mediaType,
		viewOffset: sessionItem.viewOffset
	};
	return plexVideoItem;
}

function getExpectedMovie(): SessionItem {
	const mediaInfo: MediaMetadata = {
		displayTitle: "Great Movie",
		duration: 10,
		key: "key",
		librarySectionId: 1,
		librarySectionKey: "/library/1",
		title: "Great Movie",
		type: MediaType.Movie
	};
	const user: MediaUser = {
		id: "id",
		name: "test user",
		thumb: "user thumb"
	};
	const expectedMovie: SessionItem = {
		mediaInfo,
		user,
		viewOffset: 1
	};
	return expectedMovie;
}

function getExpectedTvShow(): SessionItem {
	const mediaInfo: MediaMetadata = {
		displayTitle: "TV Show Name - Episode Name",
		duration: 10,
		grandparentTitle: "TV Show Name",
		key: "key",
		librarySectionId: 1,
		librarySectionKey: "/library/1",
		title: "Episode Name",
		type: MediaType.TvShow,
	};
	const user: MediaUser = {
		id: "id",
		name: "test user",
		thumb: "user thumb"
	};
	const expectedTvShow: SessionItem = {
		mediaInfo,
		user,
		viewOffset: 1
	};
	return expectedTvShow;
}

function getExpectedSong(): SessionItem {
	const mediaInfo: MediaMetadata = {
		displayTitle: "Artist - Track Title",
		duration: 10,
		grandparentTitle: "Artist",
		key: "key",
		librarySectionId: 1,
		librarySectionKey: "/library/1",
		parentTitle: "Album Title",
		title: "Track Title",
		type: MediaType.Song,
	};
	const user: MediaUser = {
		id: "id",
		name: "test user",
		thumb: "user thumb"
	};
	const expectedSong: SessionItem = {
		mediaInfo,
		user,
		viewOffset: 1
	};
	return expectedSong;
}
