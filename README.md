Plex Server Dashboard

Use the following commands from the source directory to build and run:

`docker build -t plex-dash .`

`docker run -p PORTNUMBER:8080 -d plex-dash`