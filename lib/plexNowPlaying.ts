import { IConfig } from "config";
import { RequestAPI, RequiredUriUrl } from "request";
import { RequestPromise, RequestPromiseOptions } from "request-promise-native";
import { PlexAuthenticator } from "./plexAuthentication";
import { MediaFilter, MediaMetadata, MediaType, MediaUser, Server, SessionItem } from "./plexTypes";
export class NowPlaying {
	private server: Server = {};
	private plexAuthenticator: PlexAuthenticator;
	private request: RequestAPI<RequestPromise, RequestPromiseOptions, RequiredUriUrl>;
	private config: IConfig;
	constructor(
		plexAuthenticator: PlexAuthenticator,
		request: RequestAPI<RequestPromise, RequestPromiseOptions, RequiredUriUrl>,
		config: IConfig) {
		this.plexAuthenticator = plexAuthenticator;
		this.request = request;
		this.config = config;
	}

	public get(): Promise<SessionItem[]> {
		return this.plexAuthenticator.getToken().then((token) => {
			this.server.hostname = this.config.get<string>("plexMediaServer.hostname") || "localhost";
			this.server.port = this.config.get<string>("plexMediaServer.port") || "32400";
			if (!token) {
				throw new Error("Token required: Use authenticate(user) to get an authentication token");
			}
			const options = {
				headers: {
					"Content-Type": "application/x-www-form-urlencoded",
					"X-Plex-Token": token
				},
				json: true,
				resolveWithFullResponse: true,
				uri: "http://" + this.server.hostname + ":" + this.server.port + "/status/sessions"
			};
			return this.request.get(options).then((response) => {
				return Promise.resolve(response.body.MediaContainer.Video.map((video) => this.transformVideoResponse(video)));
			}).catch(() => {
				return Promise.resolve([]);
			});
		});
	}

	private transformVideoResponse(videoItem): SessionItem {
		const mediaInfo: MediaMetadata = {
			displayTitle: this.getDisplayTitle(videoItem.grandparentTitle, videoItem.title),
			duration: Number(videoItem.duration),
			key: videoItem.key,
			librarySectionId: Number(videoItem.librarySectionId),
			librarySectionKey: videoItem.librarySectionKey,
			title: videoItem.title,
			type: this.getMediaType(videoItem.type)
		};
		const user: MediaUser = {
			id: videoItem.User.id,
			name: videoItem.User.title,
			thumb: videoItem.User.thumb
		};
		const sessionItem: SessionItem = {
			mediaInfo,
			user,
			viewOffset: Number(videoItem.viewOffset)
		};
		sessionItem.mediaInfo = this.addMediaInformation(
			sessionItem.mediaInfo,
			"grandparentTitle",
			videoItem.grandparentTitle);
		sessionItem.mediaInfo = this.addMediaInformation(sessionItem.mediaInfo, "parentTitle", videoItem.parentTitle);
		return sessionItem;
	}

	private addMediaInformation(mediaInfo: MediaMetadata, key: string, value: string): MediaMetadata {
		if (value) {
			mediaInfo[key] = value;
		}
		return mediaInfo;
	}

	private getDisplayTitle(grandparentTitle: string, title: string): string {
		return grandparentTitle
			? `${grandparentTitle} - ${title}`
			: title;
	}
	private getMediaType(type: string): MediaType {
		return type === "movie"
			? MediaType.Movie
			: type === "episode"
				? MediaType.TvShow
				: MediaType.Song;
	}
}
