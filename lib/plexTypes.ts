export interface User {
	authToken: string;
	password: string;
	username: string;
}

export interface Server {
	hostname?: string;
	port?: string;
}

export enum MediaType {
	Movie,
	TvShow,
	Song
}

export interface Session {
	id: string;
	bandwidth: number;
	location: string;
}

export interface TranscodeSession {
	audioChannels: number;
	audioCodec: string;
	audioDecision: string;
	complete: boolean;
	container: string;
	context: string;
	duration: number;
	height: number;
	key: string;
	progress: number;
	protocol: string;
	remaining: number;
	sourceAudioCodec: string;
	sourceVideoCodec: string;
	speed: number;
	throttled: boolean;
	transcodeHwRequested: boolean;
	transcodeHwFullPipeline: boolean;
	videoCodec: string;
	videoDecision: string;
	width: number;
}

export interface MediaFilter {
	count?: number;
	filter: string;
	id: string;
	role?: string;
	tag: string;
}

export interface MediaPart {
	audioProfile: string;
	container: string;
	duration: number;
	file: string;
	has64bitOffsets: boolean;
	id: string;
	indexes: string;
	key: string;
	optimizedForStreaming: boolean;
	size: number;
	videoProfile: string;
}
export interface MediaSpecs {
	aspectRatio: string;
	audioChannels: number;
	audioCodec: string;
	audioProfile: string;
	bitrate: number;
	container: string;
	duration: number;
	has64bitOffsets: boolean;
	height: number;
	id: string;
	mediaPart: MediaPart;
	optimizedForStreaming: boolean;
	videoCodec: string;
	videoFramerate: string;
	videoProfile: string;
	videoResolution: string;
	width: number;
}

export interface MediaPlayer {
	address: string;
	device: string;
	machineIdentifier: string;
	model: string;
	platform: string;
	platformVersion: string;
	product: string;
	profile: string;
	remotePublicAddress: string;
	state: string;
	title: string;
	vendor: string;
	version: string;
	local: boolean;
	userID: number;
}

export interface MediaUser {
	id: string;
	name: string;
	thumb: string;
}

export interface MediaMetadata {
	addedAt?: number;
	art?: string;
	audienceRatingImage?: string;
	chapterSource?: string;
	contentRating?: string;
	country?: MediaFilter;
	director?: MediaFilter;
	displayTitle: string;
	duration: number;
	genre?: MediaFilter;
	grandparentArt?: string;
	grandparentKey?: string;
	grandparentRatingKey?: string;
	grandparentTheme?: string;
	grandparentThumb?: string;
	grandparentTitle?: string;
	guid?: string;
	index?: string;
	key: string;
	lastViewedAt?: number;
	librarySectionId: number;
	librarySectionKey: string;
	parentTitle?: string;
	primaryExtraKey?: string;
	producer?: MediaFilter;
	rating?: string;
	ratingImage?: string;
	ratingKey?: string;
	releaseDate?: Date;
	role?: MediaFilter;
	studio?: string;
	summary?: string;
	tagline?: string;
	thumb?: string;
	title: string;
	titleSort?: string;
	type: MediaType;
	updatedAt?: number;
	writer?: MediaFilter;
	year?: number;
}

export interface SessionItem {
	mediaInfo: MediaMetadata;
	player?: MediaPlayer;
	session?: Session;
	sessionKey?: string;
	transcodeSession?: TranscodeSession;
	user: MediaUser;
	viewOffset: number;
}

export interface ICache {
	get(key: string): any;
	put(key: string, value: any): any;
}
