import { RequestAPI, RequiredUriUrl } from "request";
import { RequestPromise, RequestPromiseOptions } from "request-promise-native";
import { ICache, User } from "./plexTypes";
const TOKEN_CACHE_KEY = "authToken";

export class PlexAuthenticator {
	private cacheKey: string;
	private request: RequestAPI<RequestPromise, RequestPromiseOptions, RequiredUriUrl>;
	private cache: ICache;
	constructor(
		cache: ICache,
		request: RequestAPI<RequestPromise, RequestPromiseOptions, RequiredUriUrl>) {
		this.cacheKey = TOKEN_CACHE_KEY;
		this.cache = cache;
		this.request = request;
	}
	public authenticate(user: User): Promise<string> {
		return this.getToken().then((token: string) => {
			if (token) {
				return Promise.resolve(token);
			}
			if (user.authToken) {
				return Promise.resolve(this.cache.put(this.cacheKey, user.authToken));
			}
			const options = {
				body: encodeURIComponent("user[login]=" + user.username + "&user[password]=" + user.password),
				headers: {
					"Content-Type": "application/x-www-form-urlencoded",
					"X-Plex-Client-Identifier": "plex-dash-test",
					"X-Plex-Product": "plex-dash",
					"X-Plex-Version": "0.0.1"
				},
				json: true,
				resolveWithFullResponse: true,
				uri: "https://plex.tv/users/sign_in.json"
			};
			return this.request.post(options).then((response) => {
				return this.cache.put(this.cacheKey, response.body.user.authToken);
			});
		});
	}
	public getToken(): Promise<string> {
		return Promise.resolve(this.cache.get(this.cacheKey));
	}
}
